import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { Rechnung } from './data/Rechnung';
import { RechnungStorageService } from './services/rechnung-storage.service';
import { RechnungTableComponent } from "./components/rechnung-table/rechnung-table.component";

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [CommonModule, RouterOutlet, RechnungTableComponent]
})
export class AppComponent {

}
