import { Component, signal } from '@angular/core';
import { Rechnung } from '../../data/Rechnung';
import { RechnungStorageService } from '../../services/rechnung-storage.service';

@Component({
  selector: 'app-rechnung-table',
  standalone: true,
  imports: [],
  templateUrl: './rechnung-table.component.html',
  styleUrl: './rechnung-table.component.scss'
})
export class RechnungTableComponent {
  rechnungen: Rechnung[] = this.rechnungenStorageService.getRechnungen();
  hidden = true;
  nextNumber = signal(this.rechnungen.length);

  constructor(private rechnungenStorageService: RechnungStorageService) {}

  removeRechnung(nummer: number) {
    this.rechnungen = this.rechnungenStorageService.removeRechnung(nummer);
  }

  addRechnung(n: number, d: Date, b: number, a: string) {
    this.rechnungen = this.rechnungenStorageService.addRechnung({
      nummer: n,
      datum: d,
      betrag: b,
      arzt: a
    })
  }
}
