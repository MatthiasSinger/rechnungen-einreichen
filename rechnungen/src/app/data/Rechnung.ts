export type Rechnung = {
  nummer: number;
  datum: Date;
  betrag: number;
  arzt: string;
};
