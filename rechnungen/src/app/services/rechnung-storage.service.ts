import { Injectable } from '@angular/core';
import { Rechnung } from '../data/Rechnung';

@Injectable({
  providedIn: 'root',
})
export class RechnungStorageService {
  constructor() {}

  getRechnungen(): Rechnung[] {
    let rechnungenRaw = localStorage.getItem('rechnungen');
    if (!rechnungenRaw) {
      return [];
    }
    return JSON.parse(rechnungenRaw);
  }

  addRechnung(r: Rechnung): Rechnung[] {
    let rechnungen = this.getRechnungen();
    rechnungen.push(r);
    localStorage.setItem('rechnungen', JSON.stringify(rechnungen));
    return rechnungen;
  }

  removeRechnung(nummer: number): Rechnung[] {
    let rechnungen = this.getRechnungen();
    let sizeBefore = rechnungen.length;
    let neueRechnungen = rechnungen.filter((r) => r.nummer !== nummer);
    let sizeAfter = neueRechnungen.length;
    if (sizeAfter < sizeBefore) {
      localStorage.setItem('rechnungen', JSON.stringify(neueRechnungen));
      return neueRechnungen;
    } else {
      return rechnungen;
    }
  }
}
